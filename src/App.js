import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import LoginPageForm from './components/LoginPageForm'
import Dashboard from './components/Dashboard'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route exact path="/" component={LoginPageForm}/>
        <Route exact path="/dashboard" component={Dashboard}/>
      </div>
    );
  }
}

export default App;
